# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'cito_backbone_client/version'

Gem::Specification.new do |spec|
  spec.name          = "cito_backbone_client"
  spec.version       = Cito::BackboneClient::VERSION
  spec.authors       = ["Alexander Schrot"]
  spec.email         = ["alexander@wonderweblabs.com"]

  spec.summary       = "A client for backbone"
  spec.description   = "This client for backbone is a client for backbone is a ..."
  spec.homepage      = "http://localhost"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org by setting 'allowed_push_host', or
  # delete this section to allow pushing this gem to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.10"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec"
  spec.add_development_dependency "vcr"
  spec.add_development_dependency "webmock"

  spec.add_dependency 'json', '>= 1.8.3'
  spec.add_dependency 'excon', '>= 0.45.4'
  spec.add_dependency 'ruby-hmac', '~> 0.4.0'
  # spec.add_dependency 'activemodel', '>= 5.1.0.rc2', '< 5.2'
  # spec.add_dependency 'activesupport', '>= 5.1.0.rc2', '< 5.2'
  spec.add_dependency 'activemodel', '>= 4.2.3', '< 5.2'
  spec.add_dependency 'activesupport', '>= 4.2.3', '< 5.2'
  spec.add_dependency 'devise', '>= 4.2'
  spec.add_dependency 'money', '~> 6.7.0'
end
