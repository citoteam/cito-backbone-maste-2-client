$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)
require 'byebug'
require 'vcr'

RSpec.configure do |c|
  c.filter_run_including :focus => true
  c.run_all_when_everything_filtered = true
end

VCR.configure do |c|
  c.cassette_library_dir = 'spec/cassettes'
  c.hook_into :webmock
  c.configure_rspec_metadata!
  c.default_cassette_options = { record: :new_episodes }
end

require 'cito_backbone_client'
