require 'spec_helper'

describe Cito::BackboneClient::CodeSubscription, :vcr do

  describe ".new" do

    subject{ Cito::BackboneClient::CodeSubscription.new }

    it "should handle changes on the account_id correctly" do
      subject.account_id = 1
      expect(subject.account_id).to eq "1"
      subject.account_id = 2
      expect(subject.account_id).to eq "2"
      expect(subject.changes["account_id"]).to eq ["1", "2"]
    end

    it "should handle changes on account correctly" do
      account_a = Cito::BackboneClient::Account.find(1)
      subject.account = account_a
      expect(subject.account).to eq account_a
      account_b = Cito::BackboneClient::Account.find(2)
      subject.account = account_b
      expect(subject.account).to eq account_b
      expect(subject.changes["account"]).to eq [account_a, account_b]
    end

    it "should handle changes on the code_data_id correctly" do
      subject.code_data_id = 1
      expect(subject.code_data_id).to eq "1"
      subject.code_data_id = 2
      expect(subject.code_data_id).to eq "2"
      expect(subject.changes["code_data_id"]).to eq ["1", "2"]
    end

    it "should handle changes on code_data correctly" do
      code_data_a = Cito::BackboneClient::CodeData.find(1)
      subject.code_data = code_data_a
      expect(subject.code_data).to eq code_data_a
      code_data_b = Cito::BackboneClient::CodeData.find(2)
      subject.code_data = code_data_b
      expect(subject.code_data).to eq code_data_b
      expect(subject.changes["code_data"]).to eq [code_data_a, code_data_b]
    end

  end

  describe "association access" do

    it "should be done by object reference" do
      a = Cito::BackboneClient::Account.new
      cs = Cito::BackboneClient::CodeSubscription.new
      cs.account = a
      expect(cs.account.object_id).to eq a.object_id
    end

  end

  describe "creating" do

    let(:account){ Cito::BackboneClient::Account.find(1) }
    let(:valid_attributes){
      {
        from: Date.today + 1.day,
        to: Date.today + 2.days,
        code_data_id: "1",
        code_country: "DE",
        code_string: "ABC"
      }
    }

    it "should fail without required attributes" do
      subject.save
      expect(subject.errors).to be_present
      expect(subject.id).to be_nil
      expect(subject).to_not be_persisted
    end

    it "should fail with missing attributes assigned to errors" do
      subject.save
      errors = subject.errors
      [:from, :to, :code, :code_country, :code_string, :code_data, :account].each do |key|
        expect(errors[key]).to be_present
      end
    end

    it "should succeed with required attributes" do
      subject.attributes = valid_attributes
      subject.account = account
      subject.save
      expect(subject.errors).to_not be_present
      expect(subject.id).to_not be_nil
      expect(subject.account_id.to_s).to eq account.id.to_s
      expect(subject).to be_persisted
    end

    it "should succeed with an unpersisted account" do
      account = Cito::BackboneClient::Account.new(name: "foobar")

      subject.attributes = valid_attributes
      subject.account = account
      subject.save
      expect(subject.errors).to_not be_present
      expect(subject.id).to_not be_nil
      expect(subject.account).to be_present
      expect(subject.account.id).to be_present
      expect(subject).to be_persisted
    end

    it "should succeed when assigning code data" do
      cd = Cito::BackboneClient::CodeData.new

      subject.attributes = valid_attributes.except(:code_data_id)
      subject.account = account
      subject.code_data = cd
      subject.save
      expect(subject.errors).to_not be_present
      expect(subject.id).to_not be_nil
      expect(subject.code_data).to be_present
      expect(subject.code_data.id).to be_present
      expect(subject).to be_persisted
    end

    it "should succeed when assigning code data with localized infos" do
      cd = Cito::BackboneClient::CodeData.new
      li = Cito::BackboneClient::LocalizedInfo.new(
        localization: "DE",
        description: "foobar"
      )
      cd.localized_infos = [li]

      subject.attributes = valid_attributes.except(:code_data_id)
      subject.account = account
      subject.code_data = cd
      subject.save
      expect(subject.errors).to_not be_present
      expect(subject.id).to_not be_nil
      expect(subject.code_data).to be_present
      expect(subject.code_data.id).to be_present
      expect(subject.code_data.localized_infos).to be_present
      expect(subject.code_data.localized_infos.map(&:id).compact).to be_present
      expect(subject.code_data.localized_infos.map(&:localization).compact).to eq ['DE']
      expect(subject.code_data.localized_infos.map(&:description).compact).to eq ['foobar']
      expect(subject).to be_persisted
    end

    it "should fail when assigning code data with localized infos with duplicated locales" do
      cd = Cito::BackboneClient::CodeData.new
      locale = "DE"
      li_1 = Cito::BackboneClient::LocalizedInfo.new(
        localization: locale,
        description: "foobar"
      )
      li_2 = Cito::BackboneClient::LocalizedInfo.new(
        localization: locale,
        description: "gnazgnar"
      )
      cd.localized_infos = [li_1, li_2]

      subject.attributes = valid_attributes.except(:code_data_id)
      subject.account = account
      subject.code_data = cd
      subject.save
      expect(subject.errors).to be_present
      expect(subject.errors[:localized_infos]).to be_present
      expect(subject.id).to be_nil
      expect(subject.code_data).to be_present
      expect(subject.code_data.id).to_not be_present
      expect(subject.code_data.localized_infos).to be_present
      expect(subject.code_data.localized_infos.map(&:id).compact).to_not be_present
      expect(subject).to_not be_persisted
    end

    it "should succeed when assigning code data with location data" do
      cd = Cito::BackboneClient::CodeData.new
      ld = Cito::BackboneClient::LocationDatum.new(
        location_latitude: 23,
        location_longitude: 42
      )
      cd.location_data = [ld]

      subject.attributes = valid_attributes.except(:code_data_id)
      subject.account = account
      subject.code_data = cd
      subject.save
      expect(subject.errors).to_not be_present
      expect(subject.id).to_not be_nil
      expect(subject.code_data).to be_present
      expect(subject.code_data.id).to be_present
      expect(subject.code_data.location_data).to be_present
      expect(subject.code_data.location_data.map(&:id).compact).to be_present
      expect(subject.code_data.location_data.map(&:location_latitude).compact).to eq ["23.0"]
      expect(subject.code_data.location_data.map(&:location_longitude).compact).to eq ["42.0"]
      expect(subject).to be_persisted
    end
  end

  describe "getting" do

    it "should get code data by id when existing" do
      obj = Cito::BackboneClient::CodeSubscription.find(1)
      expect(obj.id).to be 1
    end

    it "should not get code data by id when not existing" do
      expect do
        Cito::BackboneClient::CodeSubscription.find(23453434)
      end.to raise_error Cito::BackboneClient::ResourceNotFound
    end

  end

  describe "updating" do

    subject{ Cito::BackboneClient::CodeSubscription.find(1) }

    it "should succeed when changing attributes" do
      subject.priority = 42
      subject.save
      expect(subject.errors).to_not be_present
      remote = Cito::BackboneClient::CodeSubscription.find(1)
      expect(remote.priority).to eq 42
    end

    it "should fail with invalid attributes" do
      subject.code_country = "FOOBAR"
      subject.save
      expect(subject.errors).to be_present
    end

  end

  describe "destroying" do

    subject{ Cito::BackboneClient::CodeSubscription.find(1) }

    it "should succeed" do
      subject.destroy
      expect(subject.id).to be_nil
      expect(subject).to_not be_persisted
    end

  end

end
