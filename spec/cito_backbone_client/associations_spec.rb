require 'spec_helper'

describe Cito::BackboneClient::Associations do

  describe "associations" do

    describe "to-one" do

      let(:thing) do
        Class.new(Cito::BackboneClient::Resource) do
          def self.find(id)
            new(id: id, loaded: true)
          end
        end
      end

      subject do
        klass = Class.new
        klass.include(Cito::BackboneClient::Associations)
        klass.send(:association, :thing, thing, to: :one)
        klass.new
      end

      it { is_expected.to respond_to :thing }
      it { is_expected.to respond_to :thing= }
      it { is_expected.to respond_to :thing_id }
      it { is_expected.to respond_to :thing_id= }

      it "should be able to set and get #thing" do
        new_thing = thing.new(id: 1)
        subject.thing = new_thing
        expect(subject.changes).to include("thing")
        expect(subject.attributes["thing"]).to eq new_thing
        # the getter lazy loads
        expect(subject.thing.id).to eq new_thing.id
        expect(subject.thing).to be_loaded
      end

      it "should be able to set and get #thing_id" do
        new_thing_id = 1
        subject.thing_id = new_thing_id
        expect(subject.changes).to include("thing_id")
        expect(subject.attributes["thing_id"]).to eq new_thing_id.to_s
        # the getter lazy loads
        expect(subject.thing.id).to eq new_thing_id
        expect(subject.thing).to be_loaded
      end

      it "should lazy load things" do
        loaded = double('loaded', :loaded? => true, :id => 1)
        unloaded = double('unloaded', :loaded? => false, :load => loaded, :id => 1)
        subject.thing = unloaded
        expect(subject.thing).to eq loaded
      end

    end

    describe "to-many" do

      let(:thing) do
        Class.new(Cito::BackboneClient::Resource) do
          def self.find(id)
            new(id: id, loaded: true)
          end
        end
      end

      subject do
        klass = Class.new
        klass.include(Cito::BackboneClient::Associations)
        klass.send(:association, :things, thing, to: :many)
        klass.new
      end

      it { is_expected.to respond_to :things }
      it { is_expected.to respond_to :things= }
      it { is_expected.to respond_to :thing_ids }
      it { is_expected.to respond_to :thing_ids= }

      it "should be able to set and get #things" do
        new_thing = thing.new(id: 1)
        subject.things = [new_thing]
        expect(subject.changes).to include("things")
        expect(subject.attributes["things"]).to eq [new_thing]
        # the getter lazy loads
        expect(subject.things.map(&:id)).to eq [new_thing.id]
        expect(subject.things.map(&:loaded)).to eq [true]
      end

      it "should be able to set and get #thing_ids" do
        new_thing_ids = [1]
        subject.thing_ids = new_thing_ids
        expect(subject.changes).to include("thing_ids")
        expect(subject.attributes["thing_ids"]).to eq new_thing_ids.map(&:to_s)
        # the getter lazy loads
        expect(subject.things.map(&:id)).to eq new_thing_ids
        expect(subject.things.map(&:loaded)).to eq [true]
      end

      it "should lazy load things" do
        loaded = double('loaded', :loaded? => true, :id => 1)
        unloaded = double('unloaded', :loaded? => false, :load => loaded, :id => 1)
        subject.things = [unloaded]
        expect(subject.things).to eq [loaded]
      end

    end
  end

end
