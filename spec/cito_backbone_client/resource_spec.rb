require 'spec_helper'

RSpec.describe Cito::BackboneClient::Resource do

  subject do
    class Foobar < described_class
      attribute :name
    end
    Foobar
  end

  describe "changes behaviour" do

    it "has no changes on an empty initialization" do
      expect(subject.new).to_not be_changed
    end

    it "has changes on a non-empty initializtion" do
      expect(subject.new(name: "foo")).to be_changed
    end

  end
end
