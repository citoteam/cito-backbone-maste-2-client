require 'spec_helper'

RSpec.describe Cito::BackboneClient::GlobalSetting, :vcr do

  describe "getting" do

    subject{ described_class }

    it "should get code data by id when existing" do
      id = "key1"
      obj = subject.find(id)
      expect(obj.id).to eq id
    end

    it "should not get code data by id when not existing" do
      expect do
        subject.find(23453434)
      end.to raise_error Cito::BackboneClient::ResourceNotFound
    end

  end

  describe "creating" do

    it "should fail without required attributes" do
      subject.save
      expect(subject.errors).to be_present
      expect(subject.id).to be_nil
      expect(subject).to_not be_persisted
    end

    it "should succeed with required attributes" do
      subject.key = "new_key"
      subject.value = "new_value"
      subject.save
      expect(subject.errors).to_not be_present
      expect(subject.id).to_not be_nil
      expect(subject).to be_persisted
    end

    it "should clear errors after successfull save" do
      first_try = subject.save
      expect(first_try).to be false
      subject.key = "new_key"
      subject.value = "new_value"
      subject.save
      expect(subject.errors).to_not be_present
      expect(subject.id).to_not be_nil
      expect(subject).to be_persisted
    end

  end

  describe "updating" do

    let(:id){ "key1" }

    subject{ described_class.find(id) }

    it "should succeed when changing attributes" do
      new_value = "new_value"
      subject.value = new_value
      subject.save
      expect(subject.errors).to_not be_present
      remote = described_class.find(id)
      expect(remote.value).to eq new_value
    end

    it "should fail when setting required attributes to blank" do
      subject.key = nil
      subject.save
      expect(subject.errors).to be_present
    end

  end

  describe "destroying" do

    let(:id){ "key1" }

    subject{ described_class.find(id) }

    it "should succeed" do
      subject.destroy
      expect(subject.id).to be_nil
      expect(subject).to_not be_persisted
    end

  end
end
