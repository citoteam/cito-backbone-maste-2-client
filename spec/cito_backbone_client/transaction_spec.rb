require 'spec_helper'

describe Cito::BackboneClient::Transaction do

  let(:cents){ "123" }
  let(:currency){ "USD" }
  let(:comment){ "some comment" }
  let(:created_at){ "2016-01-15 10:14:38"}
  let(:transaction_attributes){
    {
      "cents": cents,
      "currency": currency,
      "comment": comment,
      "created_at": created_at
    }
  }

  subject { described_class.new(transaction_attributes) }

  it { is_expected.to respond_to :value }
  it { is_expected.to respond_to :comment }
  it { is_expected.to respond_to :created_at }

  describe "#value" do

    it "is determined by #cents and #currency" do
      expect(subject.value.cents).to eq transaction_attributes[:cents].to_i
      expect(subject.value.currency.to_s).to eq transaction_attributes[:currency]
    end

    it "is a Money" do
      expect(subject.value).to be_a Money
    end
  end

  describe "#created_at" do

    it "parses a string to a TimeWithZone" do
      expect(subject.created_at).to be_a ActiveSupport::TimeWithZone
      expect(subject.created_at.to_s).to eq "2016-01-15 11:14:38 +0100"
    end

  end

end
