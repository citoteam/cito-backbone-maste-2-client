require 'spec_helper'

describe Cito::BackboneClient::Attributes do

  describe 'provided methods' do
    subject do
      klass = Class.new
      klass.include(Cito::BackboneClient::Attributes)
      klass.send(:attribute, :name)
      klass.new
    end

    it 'should respond to #attributes' do
      expect(subject).to respond_to(:attributes)
    end

    it 'should initially have the defaults as #attributes' do
      expect(subject.attributes).to include("name" => nil)
    end

    it 'should have setted attributes in #attributes' do
      subject.name = "foo"
      expect(subject.attributes).to include("name" => "foo")
    end

    it 'should have strings as hash keys in #attributes' do
      expect(subject.attributes.keys.map(&:class).uniq).to eq [String]
    end

    it 'should enable to mass assign attributes' do
      subject.attributes = { name: "bar" }
      expect(subject.attributes).to include("name" => "bar")
    end

    it 'should track changes with direct assignment' do
      subject.name = "foo"
      expect(subject.changes).to include("name" => [nil, "foo"])
    end

    it 'should track changes with mass assignment' do
      subject.attributes = { name: "foo" }
      expect(subject.changes).to include("name" => [nil, "foo"])
    end
  end

  describe 'without defaults' do
    subject do
      klass = Class.new
      klass.include(Cito::BackboneClient::Attributes)
      klass.send(:attribute, :name)
      klass.new
    end

    it 'should respond to #name' do
      expect(subject).to respond_to(:name)
    end

    it 'should respond to #name=' do
      expect(subject).to respond_to(:name=)
    end

    it 'should have no default value' do
      expect(subject.name).to be_nil
    end

    it 'should be possible to set name' do
      subject.name = "foo"
      expect(subject.name).to eq "foo"
    end

    it "should track it attributes" do
      expect(subject.class.attribute_methods).to eq [:name]
    end

  end

  describe 'with defaults' do
    subject do
      klass = Class.new
      klass.include(Cito::BackboneClient::Attributes)
      klass.send(:attribute, :name, default: "baz")
      klass.new
    end

    it 'should respond to #name' do
      expect(subject).to respond_to(:name)
    end

    it 'should respond to #name=' do
      expect(subject).to respond_to(:name=)
    end

    it 'should have a default value' do
      expect(subject.name).to eq "baz"
    end

    it 'should be possible to set name' do
      subject.name = "foo"
      expect(subject.name).to eq "foo"
    end

  end

  describe 'with subclasses' do
    let(:klass) do
      klass = Class.new
      klass.include(Cito::BackboneClient::Attributes)
      klass.send(:attribute, :name)
      klass
    end

    subject do
      sub = Class.new(klass)
      sub.send(:attribute, :title)
      sub.new
    end

    it "should track its attributes" do
      expect(subject.class.attribute_methods).to eq [:name, :title]
      expect(klass.attribute_methods).to eq [:name]
    end
  end
end
