require 'spec_helper'

RSpec.describe Cito::BackboneClient::Product, :vcr do

  describe "fetching" do

    subject{ described_class }

    it "should get all products" do
      res = subject.fetch
      expect(res.length).to eq 4
    end

  end
end
