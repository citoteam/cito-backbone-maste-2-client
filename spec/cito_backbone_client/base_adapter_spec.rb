require 'spec_helper'

describe Cito::BackboneClient::BaseAdapter, :vcr do

  let(:id){ 1 }
  let(:klass){ Cito::BackboneClient::Account }
  let(:session){ Cito::BackboneClient::Session.default }
  let(:successfull_response){ double('successfull_response', status: 200, body: {})}

  subject{ Cito::BackboneClient::BaseAdapter.new(klass) }

  describe "#find" do

    it "should send a request to the right path" do
      expect(session).to receive(:call).with("/accounts/1", hash_including(method: :get)).and_call_original
      subject.find(id)
    end

  end

  describe "#fetch" do

    it "should send a request to the right path" do
      expect(session).to receive(:call).with("/accounts", hash_including(method: :get)).and_call_original
      subject.fetch({})
    end

  end

  describe "#save" do

    it "should send a request to the right path for updating" do
      expect(session).to receive(:call).with("/accounts/1", hash_including(method: :patch)).and_call_original
      subject.save({id: 1})
    end

    it "should send a request to the right path for creating" do
      expect(session).to receive(:call).with("/accounts", hash_including(method: :post)).and_call_original
      subject.save()
    end

  end

  describe "#delete" do

    it "should send a request to the right path" do
      expect(session).to receive(:call).with("/accounts/1", hash_including(method: :delete)).and_call_original
      subject.delete(1)
    end

  end

  describe "parameter transformations" do

    describe "on #fetch" do

      it "should let pass present paramters" do
        args = { foo: "bar" }
        expect(session).to receive(:call).with(any_args, hash_including(body: args)){ successfull_response }
        subject.fetch(args)
      end

      it "should strip empty parameters" do
        args = { foo: "" }
        expect(session).to receive(:call).with(any_args, hash_including(body: {})){ successfull_response }
        subject.fetch(args)
      end
    end

    describe "on #find" do

      it "should let pass present paramters" do
        args = { foo: "bar" }
        expect(session).to receive(:call).with(any_args, hash_including(body: args)){ successfull_response }
        subject.find(1, args) rescue nil
      end

      it "should strip empty parameters" do
        args = { foo: "" }
        expect(session).to receive(:call).with(any_args, hash_including(body: {})){ successfull_response }
        subject.find(1, args) rescue nil
      end

      describe "on #save" do

        let(:resource) do
          Cito::BackboneClient::Resource.new
        end
        let(:existing) do
          e = Cito::BackboneClient::Resource.new
          e.changes_applied
          allow(e).to receive(:id){ 23 }
          e
        end

        it "should transform to an json api conform request when creating" do
          input = {
            name: "foobar",
            to_one: resource,
            to_many: [resource],
            to_one_existing: existing
          }
          output = {
            data: {
              type: "accounts",
              attributes: {
                name: "foobar"
              },
              relationships: {
                to_one: {
                  data: {
                    type: "resources",
                    attributes: {}
                  }
                },
                to_many: [
                  {
                    data: {
                      type: "resources",
                      attributes: {}
                    }
                  }
                ],
                to_one_existing: {
                  data: {
                    type: "resources",
                    id: existing.id.to_s
                  }
                }
              }
            }
          }
          expect(session).to receive(:call).with(any_args, hash_including(body: output))
          subject.save(input) rescue nil
        end

        it "should transform to an json api conform request when updating" do
          input = {
            id: 1,
            name: "foobar",
            to_one: resource,
            to_many: [resource]
          }
          output = {
            data: {
              id: "1",
              type: "accounts",
              attributes: {
                name: "foobar"
              },
              relationships: {
                to_one: {
                  data: {
                    type: "resources",
                    attributes: {}
                  }
                },
                to_many: [
                  {
                    data: {
                      type: "resources",
                      attributes: {}
                    }
                  }
                ]
              }
            }
          }
          expect(session).to receive(:call).with(any_args, hash_including(body: output))
          subject.save(input) rescue nil
        end

      end
    end
  end

  describe "response normalization" do

    def response_with_body(status, body)
      double('successfull response', status: status, body: body.deep_stringify_keys)
    end

    describe "on #fetch" do

      it "should normalize collections without 'included'" do
        input = {
          data: [
            {
              type: "foo",
              id: "1",
              attributes: {
                name: "bar"
              },
              relationships: {
                gnaz: {
                  data: {
                    type: "gnazs",
                    id: "42"
                  }
                }
              }

            }
          ]
        }.deep_stringify_keys
        output = [
          {
            id: "1",
            name: "bar",
            loaded: true,
            gnaz: {
              id: "42",
              type: "gnazs"
            }
          }
        ].map(&:deep_stringify_keys)
        allow(session).to receive(:call){ response_with_body(200, input) }
        expect(subject.fetch[1]).to eq output

      end

      it "should normalize collections with 'included'" do
        input = {
          data: [
            {
              type: "foo",
              id: "1",
              attributes: {
                name: "bar"
              },
              relationships: {
                gnaz: {
                  data: {
                    type: "gnazs",
                    id: "42"
                  }
                }
              }

            }
          ],
          included: [
            {
              type: "gnazs",
              id: "42",
              attributes: {
                title: "blabla"
              }
            }
          ]
        }.deep_stringify_keys
        output = [
          {
            id: "1",
            name: "bar",
            loaded: true,
            gnaz: {
              id: "42",
              title: "blabla",
              loaded: true
            }
          }
        ].map(&:deep_stringify_keys)
        allow(session).to receive(:call){ response_with_body(200, input) }
        expect(subject.fetch[1]).to eq output

      end
    end

    describe "on #find and #save" do

      it "should normalize a resource without 'included'" do
        input = {
          data: {
            type: "foo",
            id: "1",
            attributes: {
              name: "bar"
            },
            relationships: {
              gnaz: {
                data: {
                  type: "gnazs",
                  id: "42"
                }
              }
            }

          }
        }.deep_stringify_keys

        output = {
          id: "1",
          name: "bar",
          loaded: true,
          gnaz: {
            id: "42",
            type: "gnazs"
          }
        }.deep_stringify_keys
        allow(session).to receive(:call){ response_with_body(201, input) }
        expect(subject.save[1]).to eq output
      end

      it "should normalize collections with 'included'" do
        input = {
          data: {
            type: "foo",
            id: "1",
            attributes: {
              name: "bar"
            },
            relationships: {
              gnaz: {
                data: {
                  type: "gnazs",
                  id: "42"
                }
              }
            }

          },
          included: [
            {
              type: "gnazs",
              id: "42",
              attributes: {
                title: "blabla"
              }
            }
          ]
        }.deep_stringify_keys

        output = {
          id: "1",
          name: "bar",
          loaded: true,
          gnaz: {
            id: "42",
            title: "blabla",
            loaded: true
          }
        }.deep_stringify_keys

        allow(session).to receive(:call){ response_with_body(201, input) }
        expect(subject.save[1]).to eq output
      end

    end

    describe "with errors" do

      it "should normalize an array to an array" do
        api_response = {
          errors: [{
            title: "foo",
            detail: "bar"
          }]
        }.deep_stringify_keys
        expected = [
          {
            key: :foo,
            value: "bar"
          }
        ].map(&:deep_stringify_keys)

        allow(session).to receive(:call){ response_with_body(422, api_response)}
        expect(subject.save[1]).to eq expected
      end

      it "should normalize a hash to an array" do
        api_response = {
          errors: {
            title: "foo",
            detail: "bar"
          }
        }.deep_stringify_keys
        expected = [
          {
            key: :foo,
            value: "bar"
          }
        ].map(&:deep_stringify_keys)

        allow(session).to receive(:call){ response_with_body(422, api_response)}
        expect(subject.save[1]).to eq expected
      end
    end
  end
end
