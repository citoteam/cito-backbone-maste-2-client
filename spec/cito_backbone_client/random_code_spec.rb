require 'spec_helper'

RSpec.describe Cito::BackboneClient::RandomCode, :vcr do

  describe "getting" do

    subject{ described_class }

    it "should get an instance" do
      res = subject.get
      expect(res).to be_a subject
    end

    it "should get for DE by default" do
      res = subject.get
      expect(res.country).to eq "DE"
    end

    it "should with default length of 6" do
      res = subject.get
      expect(res.string.to_s.length).to eq 6
    end

    it "should get with a country" do
      res = subject.get(country: "FR")
      expect(res).to be_a subject
      expect(res.country).to eq "FR"
    end

    it "should get with a length" do
      res = subject.get(length: 5)
      expect(res.string.to_s.length).to eq 5
    end

  end
end
