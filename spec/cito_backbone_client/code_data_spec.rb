require 'spec_helper'

describe Cito::BackboneClient::CodeData, :vcr do

  describe "creating" do

    it "should fail without required attributes" do
      subject.save
      expect(subject.errors).to be_present
      expect(subject.id).to be_nil
      expect(subject).to_not be_persisted
    end

    it "should succeed with required attributes" do
      subject.name = "foobar"
      subject.save
      expect(subject.errors).to_not be_present
      expect(subject.id).to_not be_nil
      expect(subject).to be_persisted
    end

  end

  describe "getting" do

    it "should get code data by id when existing" do
      obj = Cito::BackboneClient::CodeData.find(1)
      expect(obj.id).to be 1
    end

    it "should not get code data by id when not existing" do
      expect do
        Cito::BackboneClient::CodeData.find(23453434)
      end.to raise_error Cito::BackboneClient::ResourceNotFound
    end

  end

  describe "updating" do

    subject{ Cito::BackboneClient::CodeData.find(1) }

    it "should succeed when changing attributes" do
      subject.name = "foobar"
      subject.save
      expect(subject.errors).to_not be_present
      remote = Cito::BackboneClient::CodeData.find(1)
      expect(remote.name).to eq "foobar"
    end

    it "should fail when settings required attributes to blank" do
      subject.name = ""
      subject.save
      expect(subject.errors).to be_present
    end

  end

  describe "destroying" do

    subject{ Cito::BackboneClient::CodeData.find(1) }

    it "should succeed" do
      subject.destroy
      expect(subject.id).to be_nil
      expect(subject).to_not be_persisted
    end

  end

  describe "fetching" do

    it "should get all code data" do
      res = Cito::BackboneClient::CodeData.fetch
      expect(res.length).to be 2
    end

    it "should get a filtered list when filtering" do
      res = Cito::BackboneClient::CodeData.fetch(filter: { with_id: [1] })
      expect(res.length).to be 1
    end

  end

end
