require 'spec_helper'

describe Cito::BackboneClient::Language, :vcr do

  describe "fetching" do

    it "should get all languages" do
      res = Cito::BackboneClient::Language.fetch
      expect(res.length).to be 184
    end

  end
end
