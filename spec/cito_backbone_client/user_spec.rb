require 'spec_helper'

describe Cito::BackboneClient::User, vcr: true do

  let(:email){ "foobar@example.com" }
  let(:password){ "secret123" }

  describe "creating" do

    it "should fail without required attributes" do
      subject.save
      expect(subject.errors).to be_present
      expect(subject.errors[:email]).to be_present
      expect(subject.id).to be_nil
      expect(subject).to_not be_persisted
    end

    it "should succeed with required attributes"  do
      subject.email = email
      subject.password = password
      subject.save
      expect(subject.errors).to_not be_present
      expect(subject.id).to_not be_nil
      expect(subject).to be_persisted
    end

    it "should clear errors after successfull save" do
      first_try = subject.save
      expect(first_try).to be false
      subject.email = email
      subject.password = password
      subject.save
      expect(subject.errors).to_not be_present
      expect(subject.id).to_not be_nil
      expect(subject).to be_persisted
    end

  end

  describe "getting" do

    subject{ described_class }

    it "should get code data by id when existing" do
      obj = subject.find(1)
      expect(obj.id).to be 1
    end

    it "should not get code data by id when not existing" do
      expect do
        subject.find(23453434)
      end.to raise_error Cito::BackboneClient::ResourceNotFound
    end

  end

  describe "updating" do

    subject{ described_class.find(1) }

    it "should succeed when changing attributes" do
      subject.email = "barfoo@example.com"
      subject.save
      expect(subject.errors).to_not be_present
      remote = described_class.find(1)
      expect(remote.email).to eq "barfoo@example.com"
    end

    it "should fail when settings required attributes to blank" do
      subject.email = ""
      subject.save
      expect(subject.errors).to be_present
    end

  end

  describe "destroying" do

    subject{ described_class.find(1) }

    it "should succeed" do
      subject.destroy
      expect(subject.id).to be_nil
      expect(subject).to_not be_persisted
    end

  end

  describe "fetching" do

    it "should get all code data" do
      res = Cito::BackboneClient::User.fetch
      expect(res.length).to be 2
    end

    it "should get a filtered list when filtering" do
      res = Cito::BackboneClient::User.fetch(filter: { with_id: [1] })
      expect(res.length).to be 1
    end

  end

end
