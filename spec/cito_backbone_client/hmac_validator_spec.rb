require 'spec_helper'

describe Cito::BackboneClient::HmacValidator do

  let(:session){ Cito::BackboneClient::Session.default }
  let(:args){ { signature: "foo", bar: "gnaz" } }
  let(:successfull_response){ double('successfull_response', status: 200, body: {})}
  let(:unsuccessfull_response) do
    double('unsuccessfull_response', status: 666, body: {
      "errors" => [
        {
          "title" => "signature",
          "detail" => "is invalid"
        }
      ]
    })
  end

  context "when the api return 200" do

    before :each do
      expect(session).to receive(:call).with("/validate_hmac", hash_including(method: :get)).and_return(successfull_response)
    end

    it "returns true" do
      expect(subject.validate(args)).to be true
    end

  end

  context "when the api returns something else" do

    before :each do
      expect(session).to receive(:call).with("/validate_hmac", hash_including(method: :get)).and_return(unsuccessfull_response)
    end

    it "returns false" do
      expect(subject.validate(args)).to be false
    end

    it "populates #errors" do
      subject.validate(args)
      expect(subject.errors).to be_present
    end

  end

end

