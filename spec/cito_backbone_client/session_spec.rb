require 'spec_helper'

describe Cito::BackboneClient::Session do

  describe '.default', :vcr do

    it 'should not changed by setting Cito::BackboneClient.host' do
      a = subject.class.default
      Cito::BackboneClient.host = "http://www.example.com"
      b = subject.class.default
      expect(a).to be b
    end

    it 'should generate a request' do
      expect(subject.session).to receive(:request).and_call_original
      subject.call('/')
    end

    it 'should raise an server error for 50x responses' do
      fake_response = double("fake response", status: 500)
      allow(subject.session).to receive(:request).and_return(fake_response)
      expect{ subject.call('/') }.to raise_error Cito::BackboneClient::ServerError
    end

    it 'should raise an resource not found error for 404 responses' do
      fake_response = double("fake response", status: 404)
      allow(subject.session).to receive(:request).and_return(fake_response)
      expect{ subject.call('/') }.to raise_error Cito::BackboneClient::ResourceNotFound
    end


  end

end
