require 'spec_helper'

describe Cito::BackboneClient::CodeSubscriptionGroup, :vcr do

  describe "getting" do

    it "should get code data by id when existing" do
      obj = Cito::BackboneClient::CodeSubscriptionGroup.find("DE,WWL,1,2015-01-01%2000:00:00%20+0100")
      expect(obj.id).to eq "DE,WWL,1,2015-01-01%2000:00:00%20+0100"
    end

    it "should not get code data by id when not existing" do
      expect do
        Cito::BackboneClient::CodeSubscriptionGroup.find("foobar")
      end.to raise_error Cito::BackboneClient::ResourceNotFound
    end

  end

  describe "fetching" do

    it "should get all code data" do
      res = Cito::BackboneClient::CodeSubscriptionGroup.fetch
      expect(res.length).to be 2
    end

    it "should get a filtered list when filtering" do
      res = Cito::BackboneClient::CodeSubscriptionGroup.fetch(filter: { for_code_country: "DE" })
      expect(res.length).to be 1
    end

  end

end
