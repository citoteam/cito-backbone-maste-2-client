require 'spec_helper'


RSpec.describe Cito::BackboneClient::AccountBalance, :vcr do

  subject{ described_class.new(id: 1) }

  describe "attributes" do

    it { is_expected.to respond_to :balance }
    it { is_expected.to respond_to :transactions }

  end

  describe "#balance" do

    it "corresponds to #balance_cents and #balance_currency" do
      subject.balance_cents = 123
      subject.balance_currency = "EUR"
      expect(subject.balance).to eq Money.new(123, "EUR")
    end

  end

  describe "#transactions" do

    context "initially" do

      it "is an empty array" do
        expect(subject.transactions).to eq []
      end

    end

    context "with transactions internally prepopulated" do

      let(:transaction_attributes){
        [{"cents":"123","currency":"EUR","created_at":"2016-01-15 10:14:38"},
         {"cents":"123","currency":"EUR","created_at":"2016-01-15 10:17:19"},
         {"cents":"123","currency":"EUR","created_at":"2016-01-15 10:19:02"}]
      }

      before :each do
        subject.instance_variable_get(:@attributes)["transactions"] = transaction_attributes
      end

      it "has 3 transactions assigned to" do
        expect(subject.transactions.length).to be 3
        expect(subject.transactions.map(&:class).uniq).to eq [Cito::BackboneClient::Transaction]
      end

      it "assigns the correct values" do
        transaction_attributes.each do |attr|
          expect(Cito::BackboneClient::Transaction).to receive(:new).with(attr)
        end
        subject.transactions
      end

    end

  end

  describe "#add_transaction!" do

    let(:session){ Cito::BackboneClient::Session.default }
    let(:value){ Money.new(123) }
    let(:comment){ "foobar" }
    let(:timestamp){ Time.zone.now.iso8601 }
    let(:transaction){
      Cito::BackboneClient::Transaction.new(
        value: value,
        comment: comment
      )
    }

    it "hits the api" do
      expect(session).to receive(:call).with(
        "/account_balances/#{subject.id}/transactions",
        {
          method: :post,
          body: {
            data: {
              type: "transactions",
              attributes: {
                value_cents: value.cents,
                value_currency: value.currency.to_s,
                comment: comment
              }
            }
          }
        }
      ).and_call_original
      subject.add_transaction!(transaction)
    end

    it "adds a transaction on a successfull api call" do
      expect{
        subject.add_transaction!(transaction)
      }.to change{
        subject.transactions.count
      }.by(1)
    end

  end

  describe "getting" do

    subject { described_class }

    it "should get account balance by id when existing" do
      obj = subject.find(1)
      expect(obj.id).to eq 1
    end

    it "should not get account balance by id when not existing" do
      expect do
        subject.find(123456)
      end.to raise_error Cito::BackboneClient::ResourceNotFound
    end

  end
end
