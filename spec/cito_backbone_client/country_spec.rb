require 'spec_helper'

describe Cito::BackboneClient::Country, :vcr => true do

  describe ".find" do

    subject{ Cito::BackboneClient::Country }

    it "should get an existing country" do
      res = subject.find "DE"
      expect(res).to be_a Cito::BackboneClient::Resource
      expect(res.id).to eq "DE"
    end

    it "should call the api on associations without 'include'" do
      res = subject.find("DE")
      session = Cito::BackboneClient::Session.default
      expect(session).to receive(:call).at_least(:once).and_call_original
      cs = res.code_subscriptions
      expect(cs.length).to be > 0
    end

    it "should call the api on associations with 'include'" do
      res = subject.find("DE", include: "code_subscriptions")
      session = Cito::BackboneClient::Session.default
      expect(session).to_not receive(:call)
      cs = res.code_subscriptions
      expect(cs.length).to be > 0
    end

    it "should propagate changes to the underlying model" do
      res = subject.find("DE", include: "code_subscriptions")
      expect(res.code_subscriptions).to be_present
      expect(res.code_subscription_ids).to be_present
      res.code_subscriptions = nil
      expect(res.changes).to include("code_subscriptions")
      expect(res.code_subscriptions).to_not be_present
      expect(res.code_subscription_ids).to_not be_present
    end

  end

  describe ".fetch" do

    subject{ Cito::BackboneClient::Country }

    it "should fetch a list of countries" do
      res = subject.fetch
      expect(res.length).to be > 1
      expect(res.all?{ |r| r.is_a?(Cito::BackboneClient::Resource) }).to be true
    end

    it "should filter with 'filter'" do
      res = subject.fetch(filter: { where: {iso_3166_1: ["DE"] }})
      expect(res.length).to be 1
      expect(res.first.iso_3166_1).to eq "DE"
    end

    it "should call the api on associations without 'include'" do
      res = subject.fetch(filter: { where: {iso_3166_1: ["DE"] }}).first
      session = Cito::BackboneClient::Session.default
      expect(session).to receive(:call).at_least(:once).and_call_original
      cs = res.code_subscriptions
      expect(cs.length).to be > 0
    end

    it "should not call the api on associations without 'include' after initial loading" do
      res = subject.fetch(filter: { where: {iso_3166_1: ["DE"] }}).first
      session = Cito::BackboneClient::Session.default
      cs = res.code_subscriptions
      expect(session).to_not receive(:call)
      cs = res.code_subscriptions
      expect(cs.length).to be > 0
    end

    it "should call the api on associations with 'include'" do
      res = subject.fetch(filter: { where: {iso_3166_1: ["DE"] }}, include: "code_subscriptions").first
      session = Cito::BackboneClient::Session.default
      expect(session).to_not receive(:call)
      cs = res.code_subscriptions
      expect(cs.length).to be > 0
    end
  end

  describe ".create" do

    subject{ Cito::BackboneClient::Country.new }

    it "should raise an OperationNotSupported error" do
      expect{ subject.save }.to raise_error Cito::BackboneClient::OperationNotSupported
    end
  end

  describe ".new" do

    subject{ Cito::BackboneClient::Country.new }

    it "should handle changes on the code_subscription_ids correctly" do
      subject.code_subscription_ids = [1,2,3]
      expect(subject.code_subscription_ids).to eq ["1", "2", "3"]
      subject.code_subscription_ids = [3,4,5]
      expect(subject.code_subscription_ids).to eq ["3", "4", "5"]
      expect(subject.changes["code_subscription_ids"]).to eq [["1", "2", "3"], ["3", "4","5"]]
    end

    it "should handle changes on account correctly" do
      cs_1 = Cito::BackboneClient::CodeSubscription.find(1)
      cs_2 = Cito::BackboneClient::CodeSubscription.find(2)
      cs_3 = Cito::BackboneClient::CodeSubscription.find(3)
      subject.code_subscriptions = [cs_1, cs_2]
      expect(subject.code_subscriptions).to eq [cs_1, cs_2]
      subject.code_subscriptions = [cs_2, cs_3]
      expect(subject.code_subscriptions).to eq [cs_2, cs_3]
      expect(subject.changes["code_subscriptions"]).to eq [[cs_1, cs_2], [cs_2, cs_3]]
    end

  end
end
