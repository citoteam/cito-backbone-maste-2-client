require 'spec_helper'

describe Devise::Models::RemoteAuthenticatable, :vcr do

  subject do
    User = Class.new do
      attr_accessor :id, :email, :account_id
    end
    User.send(:include, described_class )
    User.new
  end

  let(:session){ Cito::BackboneClient::Session.default }
  let(:email){ "foobar@example.com" }
  let(:password){ "secret123" }

  after do
    Object.send(:remove_const, :User)
  end

  describe "#remote_authentication" do

    let(:response){ double('response', status: nil, body: nil) }

    it "should call the api with the right params" do
      expect(session).to receive(:call).with(
        "/auth/sign_in",
        {
          :method=>:post,
          :body=> {
            :user => {:email => "foobar@example.com",
                      :password => "secret123"}
          }
        }).and_return(response)
      subject.remote_authentication(email: email, password: password)
    end

    context "with valid credentials" do

      let(:email){ "cito@example.com" }
      let(:password){ "secret123" }

      it "should return a resource" do
        res = subject.remote_authentication(email: email, password: password)
        expect(res).to be_a subject.class
      end

    end

    context "with invalid credentials" do

      let(:email){ "noreply@example.com" }

      it "should return false for invalid credentials" do
        res = subject.remote_authentication(email: email, password: password)
        expect(res).to be false
      end

    end

  end

end
