# Cito::BackboneClient

Welcome to your new gem! In this directory, you'll find the files you need to be able to package up your Ruby library into a gem. Put your Ruby code in the file `lib/cito_backbone_master_2_client`. To experiment with that code, run `bin/console` for an interactive prompt.


## Installation

Add this line to your application's Gemfile:

```ruby
gem 'cito_backbone_master_2_client'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install cito_backbone_master_2_client

## Usage

* `bin/console`

* configure the credentials:
    ```
    # default host: localhost:3000
    Cito::BackboneClient.host = "https://backbone-master-2-staging.citocode.com/"
    Cito::BackboneClient.access_id = "1aa8f12703c19088de389d0fd9f9e0005066180d4b1297216ef38b5a440f4e9c207ae3d08012c64b5df45afbfa2e1dd9f30d48412f5f6f2187407e3401c3527f"
    Cito::BackboneClient.secret = "530bb59509706515d79b9a5b6aa810276c67a3adbee5b60f6cc782d3fff5e0d2ee0e4ee6e1365c92dea17e61070ab9ba1ad8696660b6b9f621b6bcb7d167b3c9"
    ```
* query the api:
    ```
    Cito::BackboneClient::Country.fetch
    ...
    ```

### General information

All resources are `ActiveAttr::Model`s and their attributes are kept in their `@attributes`. BUT unlike `ActiveRecord::Base` models, this instance variable is not available as `#attributes` on the models and there is no real type casting. Type casting is only done on read.

Also, `#read_attribute` and `#write_attribute` are not working as expected, as they are just calling the setter `#attribute=`. This can cause trouble, if you want to set attributes. So be aware, that the following does not work:

```
def association=(value)
  write_attribute(:association, value.attributes)
end
```
Neither should you call `attribute_will_change!`, because it calls the attribute's getter (which does lazy loading). Instead you have to work with `@changed_attributes`.

The public interface for a `Resource` is:
```
.find(id)                # => find a resource by id
.fetch(filter, includes) # => fetch a resource collection, filtered by 'filter', including 'includes'
#errors                  # => #present? if last api_call was 409 or 422
#save                    # => 'true' if successful or 'false' if not
#destroy                 # => 'true' if successful or 'false' if not
#persisted?              # => 'true' if present at the API
#loaded?                 # => 'true' if filled with data from the API
```
All resources should implement the attribute `id` somehow to avoid problems.

### Fetch a collection of resources

You get a collection of resources by `filters` and `includes`. `filter`s filter the result set and `include`s eager load resources:

    Cito::BackboneClient::Country.fetch(filter: { with_id: ["DE", "FR"] }, include: "code_subscriptions")

For a list of available filter you should look at the AR models in the API project (it's all their scopes!)

### Get a resource

You can get a single resource by it's ID:

    Cito::BackboneClient::Country.find("DE")

If you want to eager load associations to avoid N+1 queries:

    Cito::BackboneClient::Country.find("DE", include: "code_subscriptions")

Attributes are lazy-loading by default:
```
c = Cito::BackboneClient::Country.find("DE") # hits the api once and fetches NO information about 'code_subscriptions'
c.code_subscriptions # hits the API N times

c = Cito::BackboneClient::Country.find("DE", include: "code_subscriptions") # hits the api twice and fetches all information about 'code_subscriptions'
c.code_subscriptions # does NOT hit the API
```

### Creating a resource

```
a = Cito::BackboneClient::Account.new
a.id           # => nil
a.save         # => false
a.errors       # => #<ActiveModel::Errors:0x007f6d19478078 @base={"loaded"=>false, "id"=>nil, "name"=>nil}, messages{:name=>["can't be blank"]}
a.name = "foobar"
a.save         # => true
a.id           # => 42
```

If a create action is not supported by the API, you get an error.
```
c = Cito::BackboneClient::Country.new
c.attributes = hash_of_attributes
c.save # => Cito::BackboneClient::OperationNotSupported: No route matches '/countries''
```

You can also assign structured objects to attributes:
```
account = Cito::BackboneClient::Account.find(id)
code_subscription = Cito::BackboneClient::CodeSubscription.new
code_subscription.account = account
...
```

### Updating a resource

```
c = Cito::BackboneClient::CodeSubscription.find(1)
c.attributes = some_hash_of_changed_attributes
c.save # => 'true' or 'false' with c.errors present
```

Only "dirty" attributes are sent back to the API.

### Destroying a resource

```
c = Cito::BackboneClient::CodeSubscription.find(1)
c.destroy # => 'false' or 'true' with c.id == none
```

### Available resources

This resources can be accessed throught the gem:

* _Account_
  * attributes:
    * `#name`
  * associations:
    * `#parent`
    * `#account_balance`
    * `#address`

* _AccountBalance_
  * attributes:
    * `#balance_cents`
    * `#balance_currency`
  * methods:
    * `#transactions`: this yields a list of the `Transaction`s belonging to this account's balance
    * `#balance`: returns a `Money` object
    * \#add_transaction!(Transaction)`: Given a `Transaction` object, this method adds a transaction to the balance

* _Address_
  * attributes:
    * `#id`
    * `#company`
    * `#title`
    * `#firstname`
    * `#name`
    * `#street`
    * `#zip_code`
    * `#city`
    * `#state`
  * association:
    * `#country`

* _ApiClient_
  * attributes:
    * `#name`
    * `#secret`
    * `#access_id`
  * assocations:
    * `#account`

* _Code_
  * attributes:
    * #country`
    * `#string`
    * `#blocked`

* _CodeData_
  * attributes:
    * `#name`
    * `#email`
    * `#phone`
    * `#website`
    * `#localized_infos`
    * `#location_data`

* _CodeSubscription_
  * attributes:
    * `#from`
    * `#to`
    * `#subscription_type`
    * `#code_country`
    * `#code_string`
    * `#price_cents`
    * `#price_currency`
    * `#priority`
    * `#expiration_duration`
    * `#deactivation_duration`
    * `#varity
  * associations:
    * `#account`
    * `#code_data`
    * `#product` (which returns always nil at the moment)
  * methods:
    * `.current_for_code(code_country, code_string, filters, includes)` returns the current code subscription for a given code, filtered by `filter` and with `includes` included (or `nil` if no such code subscription present)
    * `#price` return the price as `Money` of the `CodeSubscription`. This is what should be billed.

* _CodeSubscriptionGroup_
  * attributes:
    * `#from`
    * `#to`
    * `#code_country`
    * `#code_string`
    * `#projection`
  * associations:
    * `#account`

* _Country_
  * attributes:
    * `#iso_3166_1`
    * `#name`
    * `#default_latitude`
    * `#default_longitude`
    * `#default_iso_639_1`
    * `#default_map_zoom
  * associations:
    * `#code_subscriptions`
  * `#id` is aliased to `#iso_3166_1`

* _GlobalSetting_
  * attributes:
    * `#key`
    * `#value`
    * `#hint`

* _Language_
  * attributes:
    * `#iso_639_1`
    * `#iso_639_2`
    * `#iso_639_3`
    * `#name`

* _Product_
  * attributes:
    * `#product_id`
    * `#priority`
    * `#expiration_duration`
    * `#deactivation_duration`

* _RandomCode_
  * attributes:
    * `#country`
    * `#string`
  * methods:
    * `get(*args)` get a random code from the API. At the moment that is one, that is random, and that is not in use by one other.

* _User_
  * attributes:
    * `#email`,
    * `#password`,
    * `#password_confirmation`,
    * `#encrypted_password`,
    * `#reset_password_token`,
    * `#reset_password_sent_at`,
    * `#remember_created_at`,
    * `#sign_in_count`,
    * `#current_sign_in_at`,
    * `#last_sign_in_at`,
    * `#current_sign_in_ip`,
    * `#last_sign_in_ip`,
    * `#confirmation_token`,
    * `#confirmed_at`,
    * `#confirmation_sent_at`,
    * `#unconfirmed_email`,
    * `#failed_attempts`,
    * `#unlock_token`,
    * `#locked_at`,
    * `#created_at`,
    * `#updated_at`,
    * `#deleted_at`,
    * `#fb_uid`,
    * `#fb_oauth_token`,
    * `#fb_oauth_expires_at`,
    * `#fb_auth_hash`,
    * `#google_uid`,
    * `#google_oauth_token`,
    * `#google_oauth_refresh_token`,
    * `#google_oauth_expires_at`,
    * `#google_auth_hash`,
    * `#twitter_uid`,
    * `#twitter_oauth_token`,
    * `#twitter_oauth_secret`,
    * `#twitter_auth_hash`

### Devise integration

In theory (this is to be verified) any rails app using this gem should only include

```
devise_for :user, class_name: 'Cito::BackboneClient::User'
```

into `routes.rb`. Done :)


### Errors

There are a few custom errors, that are raised at various moments.

* `ResourceNotFound`: this error is raised, if you got back an `404` error from the server on a `GET` request

* `OperationNotSupported`: this error is raised, if got back an `404` error from the server with every other verb then `GET`

* `NotImplemented`: At the moment we are using `ActiveModel::Validations` to have access to a `#errors` object. But since there is no notion a validity of a resource other than do a request a check if you get back an object with `#errors`, at the moment this is raised, if you try to ask resource `#valid?`.

* `ServerError`: if you got a `50x` back from the server.

* `UnexpectedStatus`: this is raised, if you got any status back from the server, that you can't expect. So e.g. in a request to update a resource, your (JSONAPI) api should respond with `200` if it was successfull and `422` if not (or with `50x`). If, for whatever reason, you get back another status, this error is raised.



## Development

### General

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

### Defining new resources

For _standard_ resources the definition should be as straight forward as

```
class Cito::BackboneClient::Post < Cito::BackboneClient::Resource

  attribute :name                          # like a string, number, ....

  association :author, Author, to: :one    # this yield's a structured 'author' object

end
```

* Here, a attribute can be anything, it doesn't have to a _primitive_ object (like string, integer, boolean, ...), but can be anything. If you don't define getters and setters, it's just the API response (what possibly can be a hash, array, ...). But if you override the getters and/or setters (like e.g. in the CodeData class), you may be able to get back objects. If you do this, you may (and should) use a class inheriting from `Cito::BackboneClient::Value`, a class that also includes the attribute's and association's interface, but doesn't have any API endpoint (i.e. is no resource).

* Every resource should have as it's "ID" attribute this value, that is used as the key to get the resource from the API. So, if there's no generic ID on the resource, should should alias it to another resource. One example is the country resource, where you have an attribute `iso_3166_1`, which is a standardized code, which is also used to get the resource at `backend.example/countries/<iso_3166_1_code>`. This is why there is `alias_method :id, :iso_3166_1` and `alias_method :id=, :iso_3166_1=`

* If you define a association, you have to specify a name under which attribute is available (`:author`), a class (which is a resource), that is used to construct each association member (`Author`), and the type of the association (`to: :one` or `to: :many`). If you do this, you also get accessors via the resource's IDs (`:author_id` and `:author_id=`).

* In the standard case, the request by a `Cito::BackboneClient::BaseAdapter`, which constructs the API URL from the class' name (so the above would give `/posts`, and also the serialization and deserialization of the object's attributes and associations. If would want to implement some custom behavior, you can subclass `BaseAdapter` (and e.g. specify another request path) and connect this adapter to your resource via `self.adapter = CustomAdapter` in the resource definition.

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/cito_backbone_master_2_client.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
