module Cito::BackboneClient
  class ResourcesCollection < Array

    def initialize(arr, options = {})
      super(arr)

      @meta = options[:meta] || {}
    end

    def meta
      @meta || {}
    end

  end
end

