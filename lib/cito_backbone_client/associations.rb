module Cito::BackboneClient
  module Associations
    extend ActiveSupport::Concern
    include Attributes

    module ClassMethods

      def association(name, klass, to: :one)
        klass = klass.constantize if klass.is_a?(String)
        if to == :one
          define_to_one_association(name, klass)
        elsif to == :many
          define_to_many_association(name, klass)
        end
      end

      private def define_to_one_association(name, klass)
        # stuff concerning the @attributes hash
        # should always be done with strings as hash keys
        name = name.to_s
        name_id = "#{name}_id"

        attribute(name.to_sym)
        attribute(name_id.to_sym)

        define_method "#{name_id}=" do |id|
          old = @attributes[name_id]
          if id.nil?
            @attributes[name] = nil
            ans = @attributes[name_id] = nil
          else
            @attributes[name] = klass.new(id: id)
            ans = @attributes[name_id] = id.to_s
          end
          changed_attributes[name_id] = old unless old == ans
          ans
        end

        define_method "#{name}=" do |obj|
          old = @attributes[name]
          if obj.nil?
            @attributes[name_id] = nil
            ans = @attributes[name] = nil
          else
            obj = klass.new(obj) if obj.is_a?(Hash)
            @attributes[name_id] = obj.id.to_s
            ans = @attributes[name] = obj
          end
          changed_attributes[name] = old unless old == ans
          ans
        end

        define_method "#{name}" do
          res = @attributes[name]
          if !!res && res.id.present? && !res.loaded?
            res = res.load
            @attributes[name] = res
          end
          res
        end

      end

      private def define_to_many_association(name, klass)
        name = name.to_s
        name_ids = "#{name.singularize}_ids"

        attribute(name.to_sym)
        attribute(name_ids.to_sym)

        define_method "#{name_ids}=" do |ids|
          old = @attributes[name_ids]
          if ids.nil? || !ids.is_a?(Array)
            @attributes[name] = []
            ans = @attributes[name_ids] = []
          else
            @attributes[name] = ids.map{ |id| klass.new(id: id) }
            ans = @attributes[name_ids] = ids.map(&:to_s)
          end
          changed_attributes[name_ids] = old unless old == ans
          ans
        end

        define_method "#{name}=" do |objs|
          old = @attributes[name]
          if objs.nil? || !objs.is_a?(Array)
            @attributes[name_ids] = []
            ans = @attributes[name] = []
          else
            objs = objs.map do |obj|
              if objs.all?{ |v| v.is_a?(Hash) }
                klass.new(obj)
              else
                obj
              end
            end
            @attributes[name_ids] = objs.map(&:id).map(&:to_s)
            ans = @attributes[name] = objs
          end
          changed_attributes[name] = old unless old == ans
          ans
        end

        define_method "#{name}" do
          ress = @attributes[name]
          if ress.present?
            ress.map! do |res|
              # don't try to change !!res to res.present?
              # active_attr blows this up
              if !!res && res.id.present? && !res.loaded?
                res = res.load
              else
                res
              end
            end
            @attributes[name] = ress
          end
          ress
        end

      end
    end

  end
end
