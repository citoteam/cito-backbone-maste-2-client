module Cito::BackboneClient
  class CodeSubscriptionGroup < Resource

    attribute :from
    attribute :to
    attribute :code_country
    attribute :code_string
    attribute :projection

    association :account, Account, to: :one

    # add custom behaviour to id. This time it isn't a integer
    def id=(id)
      @attributes["id"] = id
    end

    def projection
      attrs = @attributes["projection"]
      if attrs.present?
        attrs.map do |attr|
          if attr.kind_of?(Attributes)
            Projection.new(attr.attributes)
          else
            Projection.new(attr)
          end
        end
      end
    end

  end
end

