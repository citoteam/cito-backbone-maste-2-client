module Cito::BackboneClient
  class Code < Resource

    attribute :country
    attribute :string
    attribute :blocked

    # don't store the ID as an integer like in the super class
    def id=(id)
      @attributes["id"] = id
    end

    def self.get(country, code)
      return nil unless country.present?
      return nil unless code.present?

      find("#{country.upcase}-#{code.upcase}")
    end

    def blocked?
      blocked
    end
  end
end
