module Cito::BackboneClient
  class Language < Resource

    attribute :iso_639_1
    attribute :iso_639_2
    attribute :iso_639_3
    attribute :name

  end
end
