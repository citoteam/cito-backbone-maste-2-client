module Cito::BackboneClient
  class Account < Resource

    attribute :name
    attribute :type

    association :parent, Account, to: :one
    association :account_balance, AccountBalance, to: :one
    association :addresses, Address, to: :many

    def administrator?
      type == 'administration'
    end
    alias :administration? :administrator?

  end
end
