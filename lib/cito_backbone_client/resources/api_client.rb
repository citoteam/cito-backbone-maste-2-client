module Cito::BackboneClient
  class ApiClient < Resource

    attribute :name
    attribute :secret
    attribute :access_id

    association :account, Account, to: :one

  end
end
