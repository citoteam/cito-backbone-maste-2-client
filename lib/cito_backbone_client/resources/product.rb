module Cito::BackboneClient
  class Product < Resource

    attribute :product_id
    attribute :priority
    attribute :expiration_duration
    attribute :deactivation_duration

    # since there's is no show action for products,
    # '.find' works on filtering '.fetch'
    # TODO: implement show action on backbone side
    def self.find(id)
      fetch.find{ |p| p.id == id }
    end

  end
end
