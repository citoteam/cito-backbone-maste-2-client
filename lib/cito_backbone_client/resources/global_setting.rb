module Cito::BackboneClient
  class GlobalSetting < Resource

    # override default behaviour, which is casting to integer
    def id=(id)
      @attributes["id"] = id
    end

    attribute :key
    attribute :value
    attribute :hint

  end
end
