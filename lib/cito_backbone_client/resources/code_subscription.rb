class Cito::BackboneClient::Product < Cito::BackboneClient::Resource; end

module Cito::BackboneClient
  class CodeSubscription < Resource

    attribute :from
    attribute :to
    attribute :code_country
    attribute :code_string
    attribute :price_cents
    attribute :price_currency
    attribute :priority
    attribute :expiration_duration
    attribute :deactivation_duration
    attribute :varity
    attr_reader :state

    association :account,   Account,  to: :one
    association :code_data, CodeData, to: :one

    def self.current_for_code(code_country, code_string, filter: {}, includes: nil)
      filter = filter.merge(for_code_country: code_country,
                            for_code_string: code_string,
                            for_timestamp: Time.now.to_s)
      res = fetch(filter: filter, include: includes)
      res.to_a.first
    end

    # don't regard the product_id attribute of the json reponse
    def initialize(attrs={})
      @state = attrs.delete('aasm_state')

      super(attrs.to_h)
    end

    def attributes=(hash)
      super(hash && hash.symbolize_keys.except(:state, :aasm_state))
    end

    def price
      Money.new(price_cents, price_currency)
    end

  end
end
