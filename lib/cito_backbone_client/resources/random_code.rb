module Cito::BackboneClient
  class RandomCode < Resource

    self.adapter = RandomCodeAdapter

    attribute :country
    attribute :string

    def self.get(*args)
      status, json = adapter.new(self).get(*args)
      case status
      when 200 then new(json)
      when 404 then nil
      end
    end

  end
end
