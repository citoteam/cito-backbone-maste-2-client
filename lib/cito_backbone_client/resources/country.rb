module Cito::BackboneClient
  class Country < Resource

    attribute :iso_3166_1
    attribute :name
    attribute :default_latitude
    attribute :default_longitude
    attribute :default_iso_639_1
    attribute :default_map_zoom

    association :code_subscriptions, CodeSubscription, to: :many

    alias_method :id,  :iso_3166_1
    alias_method :id=, :iso_3166_1=

  end
end
