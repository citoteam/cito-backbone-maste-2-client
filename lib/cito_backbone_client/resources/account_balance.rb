module Cito::BackboneClient
  class AccountBalance < Resource

    self.adapter = AccountBalanceAdapter

    attribute :balance_cents
    attribute :balance_currency

    attribute :transactions

    def transactions
      attrs = @attributes["transactions"]
      attrs.to_a.map do |attr|
        Transaction.new(**attr.symbolize_keys)
      end
    end

    def balance
      Money.new(balance_cents, balance_currency)
    end

    def add_transaction!(transaction)
      status, body = adapter.new(self.class).add_transaction(self, transaction)
      case status
      when 201
        cents = body["value"]["cents"]
        currency = body["value"]["currency"]

        new_transaction = {
          comment: body["comment"],
          cents: cents,
          currency: currency,
          created_at: body["value"]["created_at"]
        }
        transactions = @attributes["transactions"] ||= []
        transactions << new_transaction

        new_balance = balance + Money.new(cents, currency)
        @attributes["balance_cents"] = new_balance.cents.to_s

        new_transaction
      else false
      end
    end

  end
end
