module Cito::BackboneClient
  class CodeData < Resource

    attribute :name
    attribute :email
    attribute :phone
    attribute :website

    attribute :localized_infos
    attribute :location_data

    def location_data
      attrs = @attributes["location_data"]
      if attrs.present?
        attrs.map do |attr|
          if attr.kind_of?(Cito::BackboneClient::Attributes)
            LocationDatum.new(attr.attributes)
          else
            LocationDatum.new(attr)
          end
        end
      else
        attrs
      end
    end

    def localized_infos
      attrs = @attributes["localized_infos"]
      if attrs.present?
        attrs.map do |attr|
          if attr.kind_of?(Cito::BackboneClient::Attributes)
            LocalizedInfo.new(attr.attributes)
          else
            LocalizedInfo.new(attr)
          end
        end
      else
        attrs
      end
    end

    def localized_infos=(localized_infos)
      name = 'localized_infos'
      send("#{name}_will_change!")
      @attributes[name.to_s] = localized_infos

      @attributes["localized_infos"] = localized_infos.to_a.map do |info|
        if info.respond_to?(:attributes)
          info.attributes
        else
          info
        end
      end
    end

    def location_data=(location_data)
      name = 'location_data'
      send("#{name}_will_change!")
      @attributes[name.to_s] = location_data

      @attributes["location_data"] = location_data.to_a.map do |data|
        if data.respond_to?(:attributes)
          data.attributes
        else
          data
        end
      end
    end

    # def localized_infos_with_typecast=(localized_infos)
    #   self.localized_infos_without_typecast = localized_infos
    #   @attributes["localized_infos"] = localized_infos.to_a.map do |info|
    #     if info.respond_to?(:attributes)
    #       info.attributes
    #     else
    #       info
    #     end
    #   end
    # end
    # alias_method_chain :localized_infos=, :typecast

    # def location_data_with_typecast=(location_data)
    #   self.location_data_without_typecast = location_data
    #   @attributes["location_data"] = location_data.to_a.map do |data|
    #     if data.respond_to?(:attributes)
    #       data.attributes
    #     else
    #       data
    #     end
    #   end
    # end
    # alias_method_chain :location_data=, :typecast

  end
end
