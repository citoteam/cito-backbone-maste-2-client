module Cito::BackboneClient
  class User < Resource
    include ActiveModel::Validations #required because some before_validations are defined in devise
    extend ActiveModel::Callbacks #required to define callbacks
    #extend Devise::Models

    define_model_callbacks :validation #required by Devise

    #devise :remote_authenticatable

    attribute :email
    attribute :password
    attribute :password_confirmation
    attribute :encrypted_password
    attribute :reset_password_token
    attribute :reset_password_sent_at
    attribute :remember_created_at
    attribute :sign_in_count
    attribute :current_sign_in_at
    attribute :last_sign_in_at
    attribute :current_sign_in_ip
    attribute :last_sign_in_ip
    attribute :confirmation_token
    attribute :confirmed_at
    attribute :confirmation_sent_at
    attribute :unconfirmed_email
    attribute :failed_attempts
    attribute :unlock_token
    attribute :locked_at
    attribute :created_at
    attribute :updated_at
    attribute :deleted_at
    attribute :fb_uid
    attribute :fb_oauth_token
    attribute :fb_oauth_expires_at
    attribute :fb_auth_hash
    attribute :google_uid
    attribute :google_oauth_token
    attribute :google_oauth_refresh_token
    attribute :google_oauth_expires_at
    attribute :google_auth_hash
    attribute :twitter_uid
    attribute :twitter_oauth_token
    attribute :twitter_oauth_secret
    attribute :twitter_auth_hash

    association :account, Account, to: :one

    def self.find_by_email(email)
      return nil unless email.present?

      fetch({filter: { with_email: email}})[0]
    end

    def fb_auth_hash
      YAML.load(@attributes["fb_auth_hash"])
    end

    def fb_auth_hash=(string_or_hash)
      dump = string_or_hash.is_a?(String) ? string_or_hash : YAML.dump(string_or_hash)

      name = 'fb_auth_hash'
      send("#{name}_will_change!")
      @attributes[name.to_s] = dump

      self.fb_auth_hash
    end
    # alias_method_chain :fb_auth_hash=, :type_casting

    # def fb_auth_hash_with_type_casting=(string_or_hash)
    #   dump = string_or_hash.is_a?(String) ? string_or_hash : YAML.dump(string_or_hash)
    #   self.fb_auth_hash_without_type_casting = dump
    #   self.fb_auth_hash
    # end
    # alias_method_chain :fb_auth_hash=, :type_casting

    def google_auth_hash
      YAML.load(@attributes["google_auth_hash"])
    end

    def google_auth_hash=(string_or_hash)
      dump = string_or_hash.is_a?(String) ? string_or_hash : YAML.dump(string_or_hash)

      name = 'google_auth_hash'
      send("#{name}_will_change!")
      @attributes[name.to_s] = dump

      self.google_auth_hash
    end

    # def google_auth_hash_with_type_casting=(string_or_hash)
    #   dump = string_or_hash.is_a?(String) ? string_or_hash : YAML.dump(string_or_hash)
    #   self.google_auth_hash_without_type_casting = dump
    #   self.google_auth_hash
    # end
    # alias_method_chain :google_auth_hash=, :type_casting

    def twitter_auth_hash
      YAML.load(@attributes["twitter_auth_hash"])
    end

    def twitter_auth_hash=(string_or_hash)
      dump = string_or_hash.is_a?(String) ? string_or_hash : YAML.dump(string_or_hash)

      name = 'twitter_auth_hash'
      send("#{name}_will_change!")
      @attributes[name.to_s] = dump

      self.twitter_auth_hash
    end

    # def twitter_auth_hash_with_type_casting=(string_or_hash)
    #   dump = string_or_hash.is_a?(String) ? string_or_hash : YAML.dump(string_or_hash)
    #   self.twitter_auth_hash_without_type_casting = dump
    #   self.twitter_auth_hash
    # end
    # alias_method_chain :twitter_auth_hash=, :type_casting


    def password_valid?(password)
      session = Cito::BackboneClient::Session.default
      method  = :post
      body    = {
        user: {
          email: self.email,
          password: password
        }
      }
      res = session.call("/auth/sign_in", method: method, body: body)
      if res.status == 201
        true
      else
        false
      end
    end
  end
end
