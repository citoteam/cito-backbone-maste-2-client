module Cito::BackboneClient
  class Account < Resource; end
  class Country < Resource; end

  class Address < Resource

    attribute :id
    attribute :company
    attribute :title
    attribute :firstname
    attribute :name
    attribute :street
    attribute :zip_code
    attribute :city
    attribute :state

    association :country, Country, to: :one
    association :account, Account, to: :one

  end
end
