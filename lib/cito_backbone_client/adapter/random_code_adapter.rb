module Cito::BackboneClient
  class RandomCodeAdapter < BaseAdapter

    def initialize(klass, session = nil)
      super
      @path    = klass.name.gsub(/(?:::)?Cito::BackboneClient::/,'').singularize.underscore.sub(/^(?!\/)/,'/')
    end

    def get(**args)
      api_get(**args)
    end

    private def api_get(**args)
      body = serialize_query_params(args)
      res = session.call(path, method: :get, body: body)
      case res.status
      when 200 then [res.status, res.body]
      else raise UnexpectedStatus
      end
    end

  end
end
