module Cito::BackboneClient
  class HmacValidationAdapter < BaseAdapter

    def initialize(klass, session = nil)
      super
      @path = '/validate_hmac'
    end

    def find(id, **args)
      body = {
        hmac: { signature: id }.merge(args)
      }
      res = session.call(path, method: :get, body: body)
      case res.status
      when 200 then [res.status, nil]
      else [res.status, normalize_errors(res.body)]
      end
    end
  end
end
