require 'cito_backbone_client/adapter/base_adapter'

module Cito::BackboneClient
  class AccountBalanceAdapter < BaseAdapter

    def add_transaction(account_balance, transaction)
      transaction_attributes = {
        value_cents: transaction.value.cents,
        value_currency: transaction.value.currency.to_s,
        comment: transaction.comment
      }
      body = serialize_resource_object(klass: transaction.class, **transaction_attributes)
      res = session.call(
        "/account_balances/#{account_balance.id}/transactions",
        {
          method: :post,
          body: body
        }
      )

      case res.status
      when 201 then [res.status, normalize_resource(res.body)]
      else raise UnexpectedStatus
      end
    end
  end
end
