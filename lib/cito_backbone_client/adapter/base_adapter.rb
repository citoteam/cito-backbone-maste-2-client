module Cito::BackboneClient
  class BaseAdapter

    attr_reader :session, :path, :klass

    def initialize(klass, session = nil)
      @session = session || Session.default
      @klass   = klass
      @path    = klass.name.gsub(/(?:::)?Cito::BackboneClient::/,'').pluralize.underscore.sub(/^(?!\/)/,'/')
    end

    def fetch(**args)
      api_fetch(args)
    end

    def find(id, **args)
      api_get(id: id, **args)
    end

    def save(**attrs)
      attrs[:id] ?
        api_update(attrs) :
        api_create(attrs)
    end

    def delete(id)
      api_delete(id: id)
    end

    private def api_fetch(**args)
      body = serialize_query_params(args)
      res = session.call(path, method: :get, body: body)
      case res.status
      when 200 then [res.status, normalize_collection(res.body)]
      else raise UnexpectedStatus
      end
    end

    private def api_get(id:, **args)
      body = serialize_query_params(args)
      res = session.call("#{path}/#{id}", method: :get, body: body)
      case res.status
      when 200 then [res.status, normalize_resource(res.body)]
      else raise UnexpectedStatus
      end
    end

    private def api_create(**args)
      body = serialize_resource_object(args)
      res = session.call(path, method: :post, body: body)
      case res.status
      when 201 then [res.status, normalize_resource(res.body)]
      when 422 then [res.status, normalize_errors(res.body)]
      else raise UnexpectedStatus
      end
    end

    private def api_update(id:, **args)
      body = serialize_resource_object(id: id, **args)
      res = session.call("#{path}/#{id}", method: :patch, body: body)
      case res.status
      when 200 then [res.status, normalize_resource(res.body)]
      when 409 then [res.status, normalize_errors(res.body)]
      else raise UnexpectedStatus
      end
    end

    private def api_delete(id:, **args)
      res = session.call("#{path}/#{id}", method: :delete, body: nil)
      [res.status, nil]
    end

    private def normalize_resource(json)
      resource_object = json["data"]
      included = json["included"]
      normalize_resource_object(resource_object, included)
    end

    private def normalize_collection(json)
      resource_objects  = json.to_h["data"]
      included          = json.to_h["included"]
      meta              = json.to_h["meta"] || {}

      ::Cito::BackboneClient::ResourcesCollection.new(
        resource_objects.to_a.map{ |o| normalize_resource_object(o, included) }, {
          meta: meta
        }
      )
    end

    private def normalize_resource_object(obj, included)
      ans = obj["attributes"]
      obj["relationships"].to_a.inject(ans) do |acc, (name, relationship_object)|
        attr = normalize_resource_relationship(relationship_object, included)
        acc[name.to_s] = attr
        acc
      end
      ans.merge("id" => obj["id"], "loaded" => true)
    end

    private def normalize_resource_relationship(relationship, included)
      if relationship["data"].is_a? Array
        relationship["data"].map do |resource_identifier|
          normalize_resource_identifier(resource_identifier, included)
        end
      elsif relationship["data"].is_a? Hash
        resource_identifier = relationship["data"]
        normalize_resource_identifier(resource_identifier, included)
      else
        nil
      end
    end

    # merges in the information about relationships
    # it's the resource's task to get it as resource objects
    # (resp. it typecaster)
    # if a 'included' block was sent from the server, the methods
    # tries to extract that information, else it uses the resource
    # identifier sent with the 'data' block.
    # You can distinguish the one from the other by the presence of
    # a 'attribute' attribute
    private def normalize_resource_identifier(resource_identifier, included)
      type = resource_identifier["type"]
      id   = resource_identifier["id"]
      if included.present? && (x = included.find{ |o| o["id"] == id && o["type"] == type}).present?
        normalize_resource_object(x, nil)
      else
        resource_identifier.slice("id", "type")
      end
    end

    private def normalize_errors(obj)
      if obj["errors"].is_a?(Array)
        obj["errors"].map do |error|
          {
            "key" => (error["title"].to_sym || :base),
            "value" => error["detail"]
          }
        end
      elsif obj["errors"].is_a?(Hash)
        error = obj["errors"]
        [{
          "key" => (error["title"].to_sym || :base),
          "value" => error["detail"]
        }]
      else
        []
      end
    end

    private def api_type(klass)
      klass.name.underscore.sub(/cito\/backbone_client\//,'').pluralize
    end


    private def serialize_resource_object(id: nil, klass: self.klass, **attrs)
      ans = {
        data: {
          type: api_type(klass),
          attributes: {},
          relationships: {}
        }
      }
      ans[:data][:id] = id.to_s if id

      ans = attrs.inject(ans) do |acc, (key, value)|
        if value.is_a?(Resource)
          acc[:data][:relationships][key] = serialize_resource_object(**value.attributes.symbolize_keys.merge(klass: value.class, id: value.id))
        elsif value.is_a?(Array) && value.all?{ |v| v.is_a?(Resource) }
          acc[:data][:relationships][key] = serialize_resource_collection(value)
        else
          acc[:data][:attributes][key] = value unless key.to_s == "loaded"
        end
        acc
      end

      ans[:data].delete(:attributes) if ans[:data][:attributes].empty? && id.present?
      ans[:data].delete(:relationships) if ans[:data][:relationships].empty?
      ans
    end

    private def serialize_resource_collection(objs)
      objs.map{ |obj| serialize_resource_object(**obj.attributes.symbolize_keys.merge(klass: obj.class)) }
    end

    private def serialize_query_params(**args)
      args.reject{ |k,v| v.blank? }
    end

  end

end
