module Cito::BackboneClient

  class Error < ::StandardError; end

  class OperationNotSupported < Error; end
  class ServerError < Error; end
  class NotImplemented < Error; end
  class ResourceNotFound < Error; end
  class UnexpectedStatus < Error; end

end
