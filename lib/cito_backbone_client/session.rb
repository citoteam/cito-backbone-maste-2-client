require 'json'
require 'excon'

require 'cito_backbone_client'

module Cito::BackboneClient
  class Session
    class << self
      attr_writer :default

      def default
        @default ||= new()
      end
    end

    attr_reader :session

    def initialize(host=Cito::BackboneClient.host)
      @session = Excon.new(host)
    end

    # this calls the api and returns a struct with
    # #status and #body.
    # #status is guaranteed to be smaller than 500
    def call(path, method: :get, body: nil)
      method = method.to_s.upcase
      hash = {
        path: path.sub(/^(?!\/)/,'/'),
        method: method,
        headers: {
          'Content-Type' => 'application/json',
          'X_AUTH_ACCESS_ID' => Cito::BackboneClient.access_id,
          accept: 'application/vnd.cito-backbone-master-v3+json',
        }
      }

      begin
        hash.merge!({body: JSON.generate(body)})
      rescue JSON::GeneratorError
      end

      res = session.request(hash)
      res_status = res.status

      raise ServerError if res_status >= 500

      if res_status == 404
        if method == 'GET'
          raise ResourceNotFound.new("Nothing found at #{path}")
        end

        if %w(POST PUT PATCH DELETE).include?(method)
          raise OperationNotSupported.new("No route matches '#{path}''")
        end
      end

      res_body = begin
                   JSON.parse(res.body)
                 rescue JSON::ParserError
                   nil
                 end

      Struct.new(:status, :body).new(res_status, res_body)
    end
  end
end
