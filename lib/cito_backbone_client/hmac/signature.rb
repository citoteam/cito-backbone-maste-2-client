require 'cito_backbone_client/hmac/signature_generator'

module Cito::BackboneClient::HMAC
  class Signature
    attr_accessor :url
    attr_accessor :verb
    attr_accessor :secret
    attr_accessor :payload
    attr_accessor :access_id

    def generated_signature
      return false if url.nil?
      return false if verb.nil?
      return false if secret.nil?

      generator = HMAC::SignatureGenerator.new(secret)
      generator.sign(verb, url, payload)
    end
  end
end
