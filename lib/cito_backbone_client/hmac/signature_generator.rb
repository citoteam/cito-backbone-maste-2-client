require 'base64'
require 'hmac'
require 'hmac-sha2'

module Cito::BackboneClient::HMAC
  class SignatureGenerator
    attr_accessor :key

    def initialize(key)
      @key = key
    end

    #
    # body: string
    # url: string
    # verb: string
    def sign(verb, url, body)
      query_params ||= {}

      string_to_sign = [
        verb,
        url,
        body,
      ].join('')

      hmac.update(string_to_sign)

      hmac.to_s
    end

    private

    def hmac
      @hmac ||= HMAC::SHA256.new(key)
    end
  end
end
