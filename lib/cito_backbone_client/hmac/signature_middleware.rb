require 'json'
require 'uri'
require 'excon/utils'
require 'excon/middlewares/base'
require 'cito_backbone_client'
require 'cito_backbone_client/hmac/signature_generator'

module Cito::BackboneClient::HMAC
  class SignatureMiddleware < ::Excon::Middleware::Base

    def request_call(datum)
      add_time(datum)
      add_signature(datum)

      @stack.request_call(datum)
    end

    private def add_time(datum)
      key = :time
      time = Time.now.to_i

      if get?(datum) || delete?(datum)
        query = datum[:query] || ''
        query = URI.decode_www_form(query) << [key, time]

        datum[:query] = URI.encode_www_form(query)
      else
        hash = JSON.parse(datum[:body] || '{}')
        hash[key] = time

        datum[:body] = JSON.generate(hash)
      end
    end

    private def add_signature(datum)
      method = datum[:method]
      body = datum[:body] || ''
      url = '' << datum[:scheme] << '://' << datum[:host] << datum[:path] << Excon::Utils.query_string(datum)

      datum[:headers]['X_AUTH_SIGNATURE'] = generated_signature(url, method, body)
    end

    private def get?(datum)
      method = datum[:method] || ''

      method == 'GET' || method == :get
    end

    private def delete?(datum)
      method = datum[:method] || ''

      method == 'DELETE' || method == :delete
    end

    private def generated_signature(url, verb, payload = '')
      return nil if url.nil?
      return nil if verb.nil?

      signature_generator ||= SignatureGenerator.new(Cito::BackboneClient.secret)
      signature_generator.sign(verb, url, payload)
    end
  end
end
