require 'cito_backbone_client/adapter/base_adapter'

module Cito::BackboneClient
  class Resource
    include ::Cito::BackboneClient::Attributes
    include ::Cito::BackboneClient::Associations
    include ::ActiveModel::Serialization

    class_attribute :adapter
    self.adapter = BaseAdapter

    attribute :id
    # store id's always as integers. This makes life a lot easier
    def id=(id)
      @attributes["id"] = id.present? ? id.to_i : id
    end

    attribute :loaded, default: false
    def loaded?
      !!loaded
    end

    def self.find(id, **args)
      status, json = adapter.new(self).find(id, args)
      case status
      when 200 then new(json).tap(&:changes_applied)
      when 404 then nil
      end
    end

    def self.fetch(filter: nil, include: nil)
      _, response = adapter.new(self).fetch(filter: filter, include: include)

      ::Cito::BackboneClient::ResourcesCollection.new(
        response.map{ |attr| new(attr).tap(&:changes_applied) },
        { meta: response.meta }
      )
    end

    # let this method of ActiveModel::Dirty be public
    public :changes_applied

    def valid?
      raise NotImplemented
    end

    def save
      errors.clear
      status, body = self.class.adapter.new(self.class).save(attributes_for_api_call)
      case status
      when 200, 201
        self.attributes = body
        changes_applied
        true
      when 409, 422
        body.each do |error|
          errors.add(error["key"], error["value"])
        end
        false
      else
        false
      end
    end

    def destroy
      status, _ = self.class.adapter.new(self.class).delete(id)
      ok = status == 204
      self.id = nil if ok
      ok
    end

    def load
      return false unless id.present?
      self.class.find(id)
    end

    # include only the changed attributes and an id, if present,
    # to determine wether to create or update
    private def attributes_for_api_call
      ans = changes.inject({}) do |acc, (key, values)|
        acc[key.to_sym] = values[1]
        acc
      end
      ans[:id] = id if id.present?
      ans
    end

    # this is somewhat of a utility function, because active_attr overrides
    # `respond_to` with a method that actually calls methods, which causes
    # e.g. hirb to call the api
    def inspect
      @attributes.inspect
    end

    # this is somewhat of a utility function, because active_attr overrides
    # `respond_to` with a method that actually calls methods, which causes
    # e.g. hirb to call the api
    # original implementation in active_model/lib/active_model/attribute_methods.rb:461
    protected def attribute_method?(attr_name) #:nodoc:
      respond_to_without_attributes?(:attributes) && @attributes.to_h.include?(attr_name)
    end

  end
end
