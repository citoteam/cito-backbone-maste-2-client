module Cito::BackboneClient
  class HmacValidator
    include ActiveModel::Validations

    attr_reader :session, :adapter

    def initialize(session = nil, adapter=HmacValidationAdapter)
      @session = session || Session.default
      @adapter = adapter
    end

    def validate(signature:, **args)
      errors.clear
      status, body = adapter.new(self.class, session).find(signature, **args)
      if status == 200
        true
      else
        body.each do |error|
          errors.add(error["key"], error["value"])
        end
        false
      end
    end

  end
end
