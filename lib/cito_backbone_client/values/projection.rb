module Cito::BackboneClient
  class Projection < Value

    attribute :code_subscription_id
    attribute :from
    attribute :to

  end
end
