module Cito::BackboneClient
  class LocalizedInfo < Value

    attr_reader :localization
    attr_reader :description
    attr_reader :fallback

    attribute :id

    def initialize(args={})
      args = args.symbolize_keys
      @localization = args.delete(:localization)
      @description  = args.delete(:description)
      @fallback  = args.delete(:fallback)
      super(args)
    end

    def attributes
      super.to_h.merge(
        localization: localization,
        description: description,
        fallback: fallback,
      )
    end

  end
end
