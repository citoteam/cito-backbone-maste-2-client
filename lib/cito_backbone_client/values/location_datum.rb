require 'cito_backbone_client/values/location'

module Cito::BackboneClient
  class LocationDatum < Value

    attr_reader :location_latitude, :location_longitude

    attribute :id

    def initialize(args={})
      args = args.symbolize_keys
      @location_latitude  = args.delete(:location_latitude)
      @location_longitude = args.delete(:location_longitude)
      super(args)
    end

    def attributes
      super.to_h.merge(
        location_latitude: location_latitude,
        location_longitude: location_longitude
      )
    end

    def location
      if location_latitude.present? && location_longitude.present?
        Location.new(location_latitude, location_longitude)
      end
    end

  end
end
