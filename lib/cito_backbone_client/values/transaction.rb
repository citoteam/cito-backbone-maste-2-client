module Cito::BackboneClient
  class Transaction < Value

    attribute :comment
    attribute :created_at

    def initialize(value: nil, cents: nil, currency: nil, **attrs)
      super(**attrs)
      if value.present?
        cents = value.cents
        currency = value.currency.to_s
      end
      @attributes["cents"]    = cents
      @attributes["currency"] = currency
    end

    def created_at
      utc_str = @attributes["created_at"]
      DateTime.parse(utc_str).in_time_zone
    end

    def value
      Money.new(@attributes["cents"], @attributes["currency"])
    end

  end
end
