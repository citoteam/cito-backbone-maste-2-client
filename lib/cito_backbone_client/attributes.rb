module Cito::BackboneClient
  module Attributes
    extend ActiveSupport::Concern
    include ActiveModel::AttributeMethods
    include ActiveModel::Model
    include ActiveModel::Dirty

    included do |base|
      base.class_eval do
        class_attribute :attribute_methods, instance_writer: false
      end
    end

    def initialize(args={})
      self.class.attribute_methods ||= []
      default_attributes = Hash[
        attribute_methods.map(&:to_s).zip( attribute_methods.count.times.map{ nil })
      ]
      @attributes ||= default_attributes
      args.select!{ |k,_| attribute_methods.include?(k.to_sym) }
      super(args)
    end

    # this complies to the ActiveModel::AttributeMethods api
    def attributes
      @attributes.slice(*attribute_methods.map(&:to_s))
    end

    def attributes=(hash)
      hash.each do |key, value|
        send("#{key}=", value)
      end
      attributes
    end


    # in the active_model implementation this is done via the getters,
    # but here we're are managing our attributes through the @attributes
    # hash to enable lazy loading
    def attribute_change(attr)
      [changed_attributes[attr], attributes[attr]] if attribute_changed?(attr)
    end

    # this complies to the ActiveModel::Model api
    def persisted?
      try(:id).present?
    end

    module ClassMethods
      def attribute(name, **args)

        define_method "#{name}=" do |arg|
          send("#{name}_will_change!")
          @attributes[name.to_s] = arg
        end

        define_method name do
          if args.has_key?(:default)
            @attributes[name.to_s] || (@attributes[name.to_s] = args[:default])
          else
            @attributes[name.to_s]
          end
        end
        self.attribute_methods ||= []
        self.attribute_methods += [name]
        define_attribute_method name
      end
    end

  end
end
