require 'rubygems'
require 'excon'
require 'json'
require 'active_support'
require 'active_support/core_ext'
require 'active_model'
require 'devise'

require 'money'
Money.default_currency = Money::Currency.new("EUR")

module Cito
  module BackboneClient

    class << self

      DEFAULT_ACCESS_ID = "id"
      DEFAULT_SECRET = "secret"
      DEFAULT_HOST = "http://localhost:3000"
      DEFAULT_TIME_ZONE = 'Berlin'

      attr_writer :access_id, :secret, :host

      def configure
        yield self
      end

      def access_id
        @access_id ||= DEFAULT_ACCESS_ID
      end

      def secret
        @secret ||= DEFAULT_SECRET
      end

      def host
        @host ||= DEFAULT_HOST
      end

      def time_zone
        @time_zone ||= DEFAULT_TIME_ZONE
      end

      def time_zone=(zone)
        Time.zone = @time_zone = zone
      end
    end
  end
end

Time.zone = Cito::BackboneClient.time_zone

require 'cito_backbone_client/hmac'
Excon.defaults[:middlewares] << Cito::BackboneClient::HMAC::SignatureMiddleware

require 'devise/models/remote_authenticatable'
require 'devise/strategies/remote_authenticatable'

require 'cito_backbone_client/attributes'
require 'cito_backbone_client/associations'
require 'cito_backbone_client/errors'
require 'cito_backbone_client/hmac_validator'
require 'cito_backbone_client/resource'
require 'cito_backbone_client/resources_collection'
require 'cito_backbone_client/session'
require 'cito_backbone_client/value'
require "cito_backbone_client/version"

require "cito_backbone_client/adapter/account_balance_adapter"
require "cito_backbone_client/adapter/base_adapter"
require "cito_backbone_client/adapter/hmac_validation_adapter"
require "cito_backbone_client/adapter/random_code_adapter"

require 'cito_backbone_client/resources/account_balance'
require 'cito_backbone_client/resources/address'
require 'cito_backbone_client/resources/account'
require 'cito_backbone_client/resources/global_setting'
require 'cito_backbone_client/resources/api_client'
require 'cito_backbone_client/resources/code'
require 'cito_backbone_client/resources/code_data'
require 'cito_backbone_client/resources/code_subscription'
require 'cito_backbone_client/resources/product'
require 'cito_backbone_client/resources/random_code'
require 'cito_backbone_client/resources/code_subscription_group'
require 'cito_backbone_client/resources/country'
require 'cito_backbone_client/resources/language'
require 'cito_backbone_client/resources/user'
require 'cito_backbone_client/values/location'
require 'cito_backbone_client/resources/language'
require 'cito_backbone_client/values/location_datum'
require 'cito_backbone_client/values/localized_info'
require 'cito_backbone_client/values/location'
require 'cito_backbone_client/values/projection'
require 'cito_backbone_client/values/transaction'

# Devise.setup do |config|
#   config.warden do |manager|
#     manager.strategies.add(:remote, Devise::Strategies::RemoteAuthenticatable)
#     manager.default_strategies(:scope => :user).unshift :remote
#   end
# end
