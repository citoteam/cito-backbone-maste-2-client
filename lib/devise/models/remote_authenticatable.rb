require 'cito_backbone_client/session'

module Devise
  module Models
    module RemoteAuthenticatable
      extend ActiveSupport::Concern

      def remote_authentication(authentication_hash)
        session = Cito::BackboneClient::Session.default
        method  = :post
        body    = {
          user: {
            email: authentication_hash[:email],
            password: authentication_hash[:password]
          }
        }
        res = session.call("/auth/sign_in", method: method, body: body)
        if res.status == 201
          self.id = res.body['id']
          self.email = res.body['email']

          self
        else
          false
        end
      end

      module ClassMethods
        def serialize_from_session(_, attributes)
          resource = self.new

          resource.id         = attributes['id']
          resource.email      = attributes['email']
          resource.account_id = attributes['account_id']
          resource.account    = attributes['account']

          resource
        end

        def serialize_into_session(record)
          session = Cito::BackboneClient::Session.default
          res     = session.call("/users/#{record.id}", method: :get)

          [
            nil,
            {
              'id'          => record.id,
              'email'       => record.email,
              'account_id'  => res.body['data']['attributes']['account_id'],
              'account'     => res.body['data']['relationships']['account']['data'],
            }
          ]
        end

      end

    end
  end
end